<?php

/**
 * @file
 * Enables yoti login system capability.
 */

use Yoti\YotiClient;
use Yoti\ActivityDetails;

require_once __DIR__ . '/YotiHelper.php';

/**
 * Implements hook_theme().
 */
function yoti_theme($existing, $type, $theme, $path) {
  return array(
    'yoti_button' => array(
      'variables' => array(
        'app_id' => NULL,
        'scenario_id' => NULL,
        'is_linked' => FALSE,
        'button_text' => NULL,
      ),
      'path' => $path . '/templates',
      'template' => 'yoti-button',
    ),
  );
}

/**
 * Display these fields.
 *
 * @return array
 *   An array of Yoti user profile attributes
 */
function yoti_map_params() {
  return YotiHelper::getUserProfileAttributes();
}

/**
 * Implements hook_stream_wrappers().
 */
function yoti_stream_wrappers() {
  return [
    'yoti' => [
      'name' => t('Private yoti files'),
      'class' => YotiStreamWrapper::class,
      'description' => t('Yoti private files.'),
      'type' => STREAM_WRAPPERS_HIDDEN,
    ],
  ];
}

/**
 * Default files (yoti://) stream wrapper class.
 */
class YotiStreamWrapper extends DrupalLocalStreamWrapper {

  /**
   * Implements abstract public function getDirectoryPath()
   */
  public function getDirectoryPath() {
    return 'sites/all/modules/yoti/data';
  }

  /**
   * Overrides getExternalUrl().
   *
   * Return the HTML URI of a private file.
   */
  public function getExternalUrl() {
    return FALSE;
  }

}

/**
 * Implements hook_menu_alter().
 */
function yoti_menu_alter(&$items) {
  return $items;
}

/**
 * Implements hook_block_info().
 */
function yoti_block_info() {
  $blocks = [];
  $blocks['yoti_link'] = [
    'info' => t('Yoti Button'),
    'cache' => DRUPAL_NO_CACHE,
  ];

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function yoti_block_view($delta = '') {
  global $user;

  $block = [];

  // No config? no button.
  $config = YotiHelper::getConfig();
  if (!$config) {
    return $block;
  }

  switch ($delta) {
    case 'yoti_link':
      if (empty($user->uid)) {
        $button_text = YotiHelper::YOTI_LINK_BUTTON_DEFAULT_TEXT;
        $is_linked = FALSE;
      }
      else {
        $button_text = 'Link to Yoti';
        $is_linked = !empty(YotiHelper::getYotiUserProfile($user->uid));
      }

      $block['content'] = array(
        '#theme' => 'yoti_button',
        '#is_linked' => $is_linked,
        '#button_text' => check_plain($button_text),
        '#app_id' => check_plain($config['yoti_app_id']),
        '#scenario_id' => check_plain($config['yoti_scenario_id']),
        '#attached' => array(
          'css' => array(
            array(
              'data' => drupal_get_path('module', 'yoti') . '/css/yoti.css',
              'group' => CSS_DEFAULT,
              'every_page' => TRUE,
            ),
          ),
          'js' => array(
            array(
              'type' => 'inline',
              'data' => yoti_get_inline_script(),
              'scope' => 'footer',
            ),
            array(
              'type' => 'external',
              'data' => YotiHelper::YOTI_SDK_JAVASCRIPT_LIBRARY,
              'scope' => 'header',
            ),
          ),
        ),
      );
      break;
  }

  return $block;
}

/**
 * Returns the inline button JavaScript.
 *
 * @return string
 *   Inline button JavaScript,
 */
function yoti_get_inline_script() {
  $script = [];

  // If connect url starts with 'https://staging' then we are in staging mode.
  $isStaging = strpos(YotiClient::CONNECT_BASE_URL, 'https://staging') === 0;
  if ($isStaging) {
    // Base url for connect.
    $baseUrl = preg_replace('/^(.+)\/connect$/', '$1', YotiClient::CONNECT_BASE_URL);

    $script[] = sprintf('_ybg.config.qr = "%s/qr/";', $baseUrl);
    $script[] = sprintf('_ybg.config.service = "%s/connect/";', $baseUrl);
  }

  // Add init()
  $script[] = '_ybg.init();';

  return implode("\r\n", $script);
}

/**
 * Implements hook_menu().
 */
function yoti_menu() {

  $items['admin/config/people/yoti'] = [
    'title' => 'Yoti Settings',
    'description' => 'Configure required settings for Yoti integration',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['yoti_api_keys_settings'],
    'access arguments' => ['administer yoti'],
    'file' => 'yoti.admin.inc',
  ];

  $items['yoti/link'] = [
    'page callback' => 'yoti_link',
    'access callback' => 'no_yoti_login',
    'type' => MENU_CALLBACK,
    'file' => 'yoti.pages.inc',
  ];

  $items['yoti/register'] = [
    'page callback' => 'drupal_get_form',
    'page arguments' => ['yoti_register'],
    'access callback' => 'user_is_anonymous',
    'type' => MENU_CALLBACK,
    'file' => 'yoti.pages.inc',
  ];

  $items['yoti/unlink'] = [
    'href' => 'yoti/unlink',
    'title' => 'Unlink Yoti',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['yoti_unlink'],
    'access callback' => 'has_yoti_login',
    'type' => MENU_CALLBACK,
    'file' => 'yoti.pages.inc',
  ];

  $items['yoti/bin-file'] = [
    'page callback' => 'yoti_bin_file',
    'access callback' => 'user_is_logged_in',
    'type' => MENU_CALLBACK,
    'file' => 'yoti.pages.inc',
  ];

  return $items;
}

/**
 * Check if user has Yoti account.
 */
function has_yoti_login() {
  global $user;

  // Check if user already has an account.
  if ($user) {
    $dbProfile = YotiHelper::getYotiUserProfile($user->uid);
    if ($dbProfile) {
      return TRUE;
    }
  }

  return FALSE;
}

/**
 * Check if user do not have user account.
 */
function no_yoti_login() {
  global $user;

  // Check if user already has an account.
  if (!$user) {
    return TRUE;
  }

  $dbProfile = YotiHelper::getYotiUserProfile($user->uid);
  if ($dbProfile) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Implements hook_permission().
 */
function yoti_permission() {
  $items = [];
  $items['administer yoti'] = [
    'title' => 'Administer Yoti Module settings',
  ];
  $items[YotiHelper::YOTI_PERMISSION_VIEW_SELFIE] = [
    'title' => 'View Yoti selfie images',
  ];
  return $items;
}

/**
 * Implements hook_ENTITY_TYPE_view() for user entities.
 */
function yoti_user_view($account, $view_mode, $langcode) {
  global $user;
  $current = $user;

  $map = yoti_map_params();

  $dbProfile = YotiHelper::getYotiUserProfile($account->uid);
  if (!$dbProfile) {
    return;
  }

  $dbProfile = unserialize($dbProfile['data']);

  foreach ($map as $field => $label) {
    if ($field === ActivityDetails::ATTR_SELFIE && !empty($dbProfile['selfie_filename'])) {
      // Hide selfie for users that don't have access.
      if (!user_access(YotiHelper::YOTI_PERMISSION_VIEW_SELFIE) &&
        $current->uid != $account->uid
      ) {
        continue;
      }

      $selfieFullPath = YotiHelper::selfieFilePath($dbProfile['selfie_filename']);
      if (is_file($selfieFullPath)) {
        $params = [
          'field' => YotiHelper::YOTI_BIN_FIELD_SELFIE,
          'token' => drupal_get_token('yoti_selfie'),
        ];
        if (user_access(YotiHelper::YOTI_PERMISSION_VIEW_SELFIE)) {
          $params['user_id'] = $account->uid;
        }
        $selfieUrl = url('/yoti/bin-file', ['query' => $params]);
        $field_content = array(
          '#theme' => 'image',
          '#width' => '100',
          '#path' => $selfieUrl,
        );
      }
    }
    else {
      $field_content = array(
        '#markup' => isset($dbProfile[$field]) ? check_plain($dbProfile[$field]) : '<i>(empty)</i>',
      );
    }

    $field_content['#prefix'] = '<label>' . check_plain($label) . '</label>';

    $account->content['summary'][$field] = [
      '#type' => 'item',
      '#id' => 'yoti-profile-' . $field,
      'content' => $field_content,
    ];
  }

  if ($current->uid === $account->uid) {
    $account->content['summary']['yoti_unlink'] = array(
      '#theme' => 'link',
      '#text' => t('Unlink Yoti Account'),
      '#path' => 'yoti/unlink',
      '#prefix' => '<div class="yoti-connect">',
      '#suffix' => '</div>',
      '#options' => array(
        'attributes' => array(
          'class' => array('button'),
          'id' => 'yoti-unlink-button',
        ),
        'html' => FALSE,
      ),
    );
  }
}
